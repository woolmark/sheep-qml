import QtQuick 2.0

Item {
    id: sheep
    objectName: "sheep"

    x: parent.width
    y: parent.height - strechedImage.height - Math.random() * 60
    width: strechedImage.width
    height: strechedImage.height

    signal sheepJumped()

    property int jumpStartX: -1 * (y - parent.height) * 52 / 78 + (parent.width - 52) / 2
    property int jumpCount: -100

    property bool streched: true
    onStrechedChanged: {
        if (streched) {
            nonStrechedImage.opacity = 0
            strechedImage.opacity = 1
        } else {
            nonStrechedImage.opacity = 1
            strechedImage.opacity = 0
        }
    }

    Image {
        id: nonStrechedImage
        source: "sheep00.png"
        opacity: 0
        anchors.centerIn: parent
    }

    Image {
        id: strechedImage
        source: "sheep01.png"
        opacity: 1
        anchors.centerIn: parent
    }

    Timer {
        interval: 100
        running: true
        repeat: true
        onTriggered: {

            x -= 5
            if (jumpCount >= 0) {
                streched = true

                if (jumpCount < 3) {
                    y -= 3
                } else if (jumpCount < 6) {
                    y += 3
                } else {
                    jumpCount = -100
                }

                if (jumpCount == 3) {
                    sheepJumped()
                }

                jumpCount++
            } else {
                streched = !streched
            }

            if (jumpStartX - strechedImage.width < x && x < jumpStartX && jumpCount < 0) {
                jumpCount = 0
            }

            if (x < -strechedImage.width) {
                sheep.destroy()
            }
        }
    }

}

