import QtQuick 2.4
import QtQuick.Controls 1.3

ApplicationWindow {
    title: qsTr("sheep")
    width: 120
    height: 120
    minimumHeight: 120
    minimumWidth: 120
    maximumHeight: 120
    maximumWidth: 120
    visible: true

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            MenuItem {
                text: qsTr("E&xit")
                onTriggered: Qt.quit();
            }
        }
    }

    SheepForm {
        id: sheepForm
        anchors.fill: parent
    }

}
