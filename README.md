Sheep for QML
----------------------------------------------------------------------------
Sheep for QML(Qt Meta Language) is an implementation [sheep] for [QML].

License
-----------------------------------------------------------------------------
[WTFPL] 

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[QML]: https://qt-project.org/ "Qt"
[WTFPL]: http://www.wtfpl.net "WTFPL"

