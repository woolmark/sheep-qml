import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Item {
    id: form
    width: 120
    height: 120

    Rectangle {
        id: background
        color: "#9696ff"
        anchors.fill: parent
        z: 0
    }

    Rectangle {
        id: ground
        color: "#63ff63"
        anchors.top: fence.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 10
        border.width: 0
        z: 1
    }

    Image {
        id: fence
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        source: "fence.png"
        z: 2
    }

    Shepherd {
        id: shepherd
    }

    Timer {
        id: sheepAppendTimer
        interval: 100
        running: false
        repeat: true
        onTriggered: appendSheep()
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton
        onPressed: sheepAppendTimer.running = true
        onReleased: sheepAppendTimer.running = false
    }

    Component.onCompleted: appendSheep()

    function appendSheep() {
        var component = Qt.createComponent("Sheep.qml")
        if (component.status == Component.Ready) {
            var sheep = component.createObject(form, { z: 3 })
            sheep.sheepJumped.connect(shepherd.onSheepJumped)
            sheep.Component.destruction.connect(shepherd.onSheepDestruction)
        }
    }

}
