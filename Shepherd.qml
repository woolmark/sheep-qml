import QtQuick 2.0

Item {

    property int sheepCount: 0

    Text {
        id: counter
        x: 8
        y: 8
        text: qsTr(sheepCount + " sheep")
        font.pixelSize: 12
    }

    function onSheepJumped() {
        sheepCount++;
        counter.text = qsTr(sheepCount + " sheep");
    }

    function onSheepDestruction() {
        var runningSheepNumber = 0;
        for (var index in parent.children) {
            if (parent.children[index].objectName == "sheep") {
                runningSheepNumber++;
            }
        }

        if (runningSheepNumber < 2) {
            parent.appendSheep();
        }
    }

}
